package com.stachu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bicycle rower = new Bicycle("Romet",30 );
        rower.getSpeed();

        MonsterTruck potworek = new MonsterTruck("Czarny szarpacz",false);

        potworek.setEgine("Hemi");
        potworek.getEgine();

        potworek.brake();
        try {
            potworek.driving();
        } catch (com.stachu.isItHaveADriver isItHaveADriver) {
            isItHaveADriver.printStackTrace();
        }

        //polimorfizm
        Type[] imiona = new Type[10];
        imiona[0] = new Tank("Tiger");
        imiona[1] = new MonsterTruck("Czołowa biegunka",false);
        imiona[2] = new Tank("Pantera");
        imiona[3] = new Tank("Rudy");
        imiona[4] = new MonsterTruck("Nieskazitelne bezgranicze",true);

        List<String> list = new ArrayList<String>();

        for(int i=0;i<5;i++)
        {
            list.add(imiona[i].Name);
        }


        //mapowanie

        HashMap<Type, Integer> rocznik = new HashMap<>();

        rocznik.put(imiona[0], 1932);
        rocznik.put(imiona[1], 2003);
        rocznik.put(imiona[2], 1941);
        rocznik.put(imiona[3], 1932);
        rocznik.put(imiona[4], 2013);

        for(HashMap.Entry<Type,Integer> entry: rocznik.entrySet())
        {
            System.out.println(entry.getKey().Name + " Wiek: "+entry.getValue());
        }


    }
}
