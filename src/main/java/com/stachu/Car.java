package com.stachu;

/**
 * Created by Stachu on 21.01.2017.
 */
abstract public class Car {

    protected String Egine;
    protected int NumberOfWheels;
    protected int NumberOfDoors;
    protected String KindOfChassis;
    protected String Name;

    public Car()
    {}



    public void setKindOfChassis(String kindOfChassis) {
        this.KindOfChassis = kindOfChassis;
    }

    public String getKindOfChassis() {

        return KindOfChassis;
    }



    public void setEgine(String egine) {
        Egine = egine;
    }



    public void setNumberOfDoors(int numberOfDoors) {
        NumberOfDoors = numberOfDoors;
    }

    public String getEgine() {

        return Egine;
    }

    public void setNumberOfWheels(int numberOfWheels) {
        NumberOfWheels = numberOfWheels;
    }

    public int getNumberOfWheels() {

        return NumberOfWheels;
    }

    public int getNumberOfDoors() {
        return NumberOfDoors;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {

        Name = name;
    }
}
