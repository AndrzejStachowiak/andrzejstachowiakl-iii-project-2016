package com.stachu;

/**
 * Created by Stachu on 21.01.2017.
 */
public interface IsBroken{

    boolean getBroken();
    void setBroken(boolean broken);
    default void brake() throws isItHaveADriver
    {
        if(getBroken())
        {
           throw new isItHaveADriver();
        }
        else
        {
            throw new isItHaveADriver();
    }
    }

}
