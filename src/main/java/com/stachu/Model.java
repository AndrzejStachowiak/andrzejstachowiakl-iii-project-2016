package com.stachu;

/**
 * Created by Stachu on 21.01.2017.
 */
public abstract class Model extends Type{

    protected String Mark;
    protected String Name;
    protected String Color;



    public void setMark(String mark) {
        Mark = mark;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getMark() {

        return Mark;
    }

    public String getName() {
        return Name;
    }

    public String getColor() {
        return Color;
    }
}
