package com.stachu;

/**
 * Created by Stachu on 22.01.2017.
 */
public class MonsterTruck extends Type implements IsBroken{

    boolean broke;

    public MonsterTruck(String name, boolean broke) {

        this.broke = broke;
        this.Name = name;

    }

    @Override
    public boolean getBroken() {
        return this.broke;
    }

    @Override
    public void setBroken(boolean broken) {
        this.broke = broken;
           }

    @Override
    public void brake() {
            this.broke = broke;
    }

    @Override
    public void driving() throws isItHaveADriver {
        if(this.broke) {
            System.out.println("Nie jedzi");
        }


    }
}
