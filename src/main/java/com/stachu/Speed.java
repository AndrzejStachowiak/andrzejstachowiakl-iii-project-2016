package com.stachu;

/**
 * Created by Stachu on 21.01.2017.
 */
public interface Speed {

    int getSpeed();

    default void isHeFast()
    {
        if(getSpeed()>250)
        {
            System.out.println("Paul Walker will be dead");
        }
        else
        {
            System.out.println("You will watch another Fast and Fourios with Paul Walker");
        }
    }

}
